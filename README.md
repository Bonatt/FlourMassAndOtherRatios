### If I have 100 g white flour, how much bran(/gluten/whatever) do I need to add to make it 15% bran?
 1. If I want 100 g total, how much of each?
 2. If I have 100 g white flour, how much bran?
 3. Repeat with the addition of some percent of germ.

```
 If I want white flour to be 15.0% bran (85.0% white flour) by weight...
 1. If I want 100.0 g total, how much of each?    -->  85.0 g white flour + 15.0 g bran = 100.0 g.
 2. If I have 100.0 g white flour, how much bran? --> 100.0 g white flour + 17.6 g bran = 117.6 g.

 If I want white flour to be 14.0% bran and 2.5% germ (83.5% white flour) by weight...
 3.1. If I want 100.0 g total, how much of each?             -->  83.5 g white flour + 14.0 g bran + 2.5 g germ = 100.0 g.
 3.2. If I have 100.0 g white flour, how much bran and germ? --> 100.0 g white flour + 16.8 g bran + 3.0 g germ = 119.8 g.

 For reference, my sourdough is 71.4% hydration, 1.7% salt (baker's percentages):
  200 g starter
  400 g water
  600 g flour
   12 g salt
```


### Mixing whipping cream and half & half together. How much to add to get 30% milk fat?
I've read somewhere that 30% MF was the minimum (or ideal?) required to make good whipped cream. I had cream and a little Half & Half, and wanted to finish the Half & Half.

```
 If I have 100.0 g Cream (35.0% MF), how much Half & Half (10.0% MF) to make 30.0% MF? --> 100.0 g Cream + 25.0 g Half & Half.
```
