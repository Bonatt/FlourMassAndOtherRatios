
### If I have 100 g white flour, how much bran(/gluten/whatever) do I need to add to make it 15% bran?
# 1. If I want 100 g total, how much of each?
# 2. If I have 100 g white flour, how much bran?
# 3. Repeat with the addition of some percent of germ.




# http://www.thefreshloaf.com/node/26724/whats-proportion-endosperm-bran-germ
bp = 15./100					# Bran percent/100. mass
ep = (1-bp)                                    	# Endosperm (white flour) percent/100. mass
print()
print('If I want white flour to be', str(round(bp*100,1))+'% bran ('+str(round(ep*100,1))+'% white flour) by weight...')


# 1. If I want "t1" g total, how much of each?
# http://www.kingarthurflour.com/professional/bakers-percentage.html
t1 = 100.					# Grams total
e1 = t1*ep					# Grams endosperm (white flour)
b1 = t1*bp					# Grams bran
print(' 1. If I want',t1,'g total, how much of each?    --> ',round(e1,1),'g white flour +',round(b1,1),'g bran =',round(t1,1),'g.')


# 2. If I have "e2" g white flour, how much bran?
# http://www.wolframalpha.com/input/?i=solve+b%2F(b%2Be)+%3D+p+for+b
e2 = 100.
b2 = e2*bp/(1-bp)
t2 = e2+b2
print(' 2. If I have',e2,'g white flour, how much bran? -->',round(e2,1),'g white flour +',round(b2,1),'g bran =',round(t2,1),'g.')




# 3. Repeat for the addition of some percent of germ.
bp = 14./100
gp = 2.5/100					# Germ percent/100. mass
ep = (1-bp-gp)
print()
print('If I want white flour to be', str(round(bp*100,1))+'% bran and '+str(round(gp*100,1))+'% germ ('+str(round(ep*100,1))+'% white flour) by weight...')


# 3.1. If I want "t31" g total, how much of each?
t31 = 100.
g31 = t31*gp
b31 = t31*bp
e31 = t31*ep
print(' 3.1. If I want',t31,'g total, how much of each?             --> ',round(e31,1),'g white flour +',round(b31,1),'g bran +',round(g31,1),'g germ =',round(t31,1),'g.')


# 3.2. If I have "e32" g white flour, how much bran and germ?
e32 = 100.
b32 = e32*bp/(1-bp-gp) 
g32 = e32*gp/(1-bp-gp)
t32 = e32+b32+g32
print(' 3.2. If I have',e32,'g white flour, how much bran and germ? -->',round(e32,1),'g white flour +',round(b32,1),'g bran +',round(g32,1),'g germ =',round(t32,1),'g.')








### For reference, my sourdough is what percent hydration?...
s = 200.					    # Starter mass
w = 400.					    # Water mass
f = 600.					    # Flour mass
salt = 12 #10.					    # Salt mass

water = s/2. + w				    # All water mass
flour = s/2. + f				    # All flour mass

print()
print('For reference, my sourdough is', str(round(water/flour*100,1))+'% hydration,', str(round(salt/flour*100,1))+'% salt (baker\'s percentages):')
print(' ', int(s), 'g starter')
print(' ', int(w), 'g water')
print(' ', int(f), 'g flour')
print('  ', int(salt), 'g salt')










### Mixing whipping cream and half & half together. Now much to add to get 30% milk fat?
# http://slideplayer.com/slide/9377793/28/images/10/Total+moles+CFinal+=+Total+volume+(C1V1+++C2V2+++%E2%80%A6)+CFinal+=.jpg
#  = C_f = (C_1*V_1 + C_2*V_2 + ...)/(V_1 + V_2 + ...)

fp = 0.30	# Final milk fat percent. We want this for final mixed.
cp = 0.35	# Cream
hhp = 0.10	# Half and half

cm = 100.	# Cream mass (not volume...)
#hhm = 100.	# Half and half. We are trying to find this given some value for "cm".

# fp = (cp*cm+hhp*hhm)/(cm+hhm)
hhm = cm*(cp-fp)/(fp-hhp)

print()
print()
print(' If I have',cm,'g Cream ('+str(cp*100)+'% MF), how much Half & Half ('+str(hhp*100)+'% MF) to make', str(fp*100)+'% MF? -->',round(cm,1),'g Cream +',round(hhm,1),'g Half & Half.')# =',round(cm+hhm,1),'g.')
print()





